package vlab.thongtinquyhoachthuduc.CustomDialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import vlab.thongtinquyhoachthuduc.R;
import vlab.thongtinquyhoachthuduc.Utils.BaseCustomDialog;

import static android.content.Context.MODE_PRIVATE;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.dialogNumber;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.isHelpFinished;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.popupWindowHelper;

public class MainHelpDialog extends BaseCustomDialog {
    private SharedPreferences notShowStatePref;

    public MainHelpDialog(final Context context) {
        super(context);
        setContentView(R.layout.layout_help_dialog);
        dialogNumber = 1;
        notShowStatePref = context.getSharedPreferences("notShowAgain", MODE_PRIVATE);
        TextView btnNext = (TextView) findViewById(R.id.btnNext);
        TextView btnSkip = (TextView) findViewById(R.id.btnSkip);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) saveNotShowAgainState();
                popupWindowHelper = new PopupWindow(context);
                popupWindowHelper.displaySearchHelpPopup();
                MainHelpDialog.this.dismiss();
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                if (checkBox.isChecked()) saveNotShowAgainState();
                MainHelpDialog.this.dismiss();
            }
        });
    }

    private void saveNotShowAgainState() {
        SharedPreferences.Editor editor = notShowStatePref.edit();
        editor.putBoolean("notShowAgain", true);
        editor.commit();
    }
}
