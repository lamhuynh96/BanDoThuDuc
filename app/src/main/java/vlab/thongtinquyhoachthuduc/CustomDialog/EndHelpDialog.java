package vlab.thongtinquyhoachthuduc.CustomDialog;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vlab.thongtinquyhoachthuduc.R;
import vlab.thongtinquyhoachthuduc.Utils.BaseCustomDialog;

import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.dialogNumber;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.isHelpFinished;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.popupWindowHelper;

public class EndHelpDialog extends BaseCustomDialog {
    public EndHelpDialog(final Context context) {
        super(context);
        setContentView(R.layout.layout_end_dialog);
        dialogNumber = 5;
        TextView btnClose = (TextView) findViewById(R.id.btnClose);
        TextView btnBack = (TextView) findViewById(R.id.btnBack);
        ImageView btnclose = (ImageView) findViewById(R.id.btnclose);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                EndHelpDialog.this.dismiss();
            }
        });
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EndHelpDialog.this.dismiss();
                isHelpFinished = true;
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EndHelpDialog.this.dismiss();
                popupWindowHelper = new PopupWindow(context);
                popupWindowHelper.displayGlossaryHelpPopup();
            }
        });
    }
}

