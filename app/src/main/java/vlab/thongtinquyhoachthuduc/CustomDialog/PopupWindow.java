package vlab.thongtinquyhoachthuduc.CustomDialog;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import vlab.thongtinquyhoachthuduc.R;
import vlab.thongtinquyhoachthuduc.Utils.DeviceSizeHelper;

import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.btnHelp;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.buttonSearch;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.dialogNumber;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.isHelpFinished;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.layoutMain;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.mapType;

public class PopupWindow {
    private Context context;

    public PopupWindow(Context context) {
        this.context = context;
    }

    private void setUpPopup(final android.widget.PopupWindow popup, View layout, final View anchor) {
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setBackgroundDrawable(null);

        final View parent = layoutMain;
        parent.post(new Runnable() {
            @Override
            public void run() {
                if (anchor == btnHelp) {
                    Rect location = DeviceSizeHelper.locateView(anchor);
                    popup.showAtLocation(layoutMain, Gravity.NO_GRAVITY, location.left,
                            location.bottom - (int)(anchor.getHeight() * 1.5));
                }
                else popup.showAsDropDown(anchor, -5, 0);
            }
        });

        popup.setOutsideTouchable(false);
        popup.setTouchable(true);
        popup.setFocusable(true);
    }

    public void displaySearchHelpPopup() {
        final android.widget.PopupWindow popup = new android.widget.PopupWindow(context);
        LayoutInflater mLayoutInflater = LayoutInflater.from(context);
        View layout = mLayoutInflater.inflate(R.layout.layout_thuadat_dialog, null);
        setUpPopup(popup, layout, buttonSearch);
        TextView btnClose = (TextView) layout.findViewById(R.id.btnClose);
        ImageView btnclose = (ImageView) layout.findViewById(R.id.btnclose);
        TextView btnNext = (TextView) layout.findViewById(R.id.btnNext);
        dialogNumber = 2;

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                displayMapTypeHelpPopup();
            }
        });
    }

    public void displayMapTypeHelpPopup() {
        final android.widget.PopupWindow popup = new android.widget.PopupWindow(context);
        LayoutInflater mLayoutInflater = LayoutInflater.from(context);
        View layout = mLayoutInflater.inflate(R.layout.layout_maptype_dialog, null);
        setUpPopup(popup, layout, mapType);
        TextView btnClose = (TextView) layout.findViewById(R.id.btnClose);
        ImageView btnclose = (ImageView) layout.findViewById(R.id.btnclose);
        TextView btnNext = (TextView) layout.findViewById(R.id.btnNext);
        TextView btnBack = (TextView) layout.findViewById(R.id.btnBack);
        dialogNumber = 3;

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                displayGlossaryHelpPopup();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                displaySearchHelpPopup();
            }
        });
    }

    public void displayGlossaryHelpPopup() {
        final android.widget.PopupWindow popup = new android.widget.PopupWindow(context);
        LayoutInflater mLayoutInflater = LayoutInflater.from(context);
        View layout = mLayoutInflater.inflate(R.layout.layout_chugiai_dialog, null);
        setUpPopup(popup, layout, btnHelp);
        TextView btnClose = (TextView) layout.findViewById(R.id.btnClose);
        ImageView btnclose = (ImageView) layout.findViewById(R.id.btnclose);
        TextView btnNext = (TextView) layout.findViewById(R.id.btnNext);
        TextView btnBack = (TextView) layout.findViewById(R.id.btnBack);
        dialogNumber = 4;

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpFinished = true;
                popup.dismiss();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                EndHelpDialog endHelpDialog = new EndHelpDialog(context);
                endHelpDialog.show();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                displayMapTypeHelpPopup();
            }
        });
    }
}
