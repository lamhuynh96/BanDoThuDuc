package vlab.thongtinquyhoachthuduc.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import vlab.thongtinquyhoachthuduc.R;


public class DetailListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] typeLand;
    private final String[] colorInHex;

    public DetailListAdapter(Context _context, String[] _typeLand, String[] _colorInHex) {
        super(_context, R.layout.list_single_detail, _typeLand);
        context = _context;
        typeLand = _typeLand;
        colorInHex = _colorInHex;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View rowView= inflater.inflate(R.layout.list_single_detail, null);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        txtTitle.setText(typeLand[position]);
        View rectView = rowView.findViewById(R.id.myRectangleView);
        rectView.setBackgroundColor(Color.parseColor(colorInHex[position]));
        return rowView;
    }
}
