package vlab.thongtinquyhoachthuduc.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import vlab.thongtinquyhoachthuduc.Model.QHInfo;
import vlab.thongtinquyhoachthuduc.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class QuyHoachListAdapter extends ArrayAdapter<QHInfo> {
    private ArrayList<QHInfo> quyHoachList;
    private QHInfo quyHoachItem;

    private static class ViewHolder {
        private TextView tvSTT;
        private TextView tvOPho;
        private TextView tvChucNangSD;
        private TextView tvChucNangSD1;
        private TextView tvSTT1;
        private TextView tvOPho1;
        private TextView tvDienTich1;
        private TextView tvTyLe1;
        private TextView tvDienTich;
        private TextView tvTyLe;
        private LinearLayout layoutQHItem;
    }

    public QuyHoachListAdapter (Context context, ArrayList<QHInfo> quyHoachList) {
        super(context, 0, quyHoachList);
        this.quyHoachList = quyHoachList;
    }

    @Override
    public int getCount() {
        return quyHoachList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        quyHoachItem = quyHoachList.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_quyhoach, parent, false);
            viewHolder.tvSTT = (TextView) convertView.findViewById(R.id.tvSTT);
            viewHolder.tvOPho = (TextView) convertView.findViewById(R.id.tvOPho);
            viewHolder.tvSTT1 = (TextView) convertView.findViewById(R.id.tvSTT1);
            viewHolder.tvOPho1 = (TextView) convertView.findViewById(R.id.tvOPho1);
            viewHolder.tvChucNangSD = (TextView) convertView.findViewById(R.id.tvChucNangSD);
            viewHolder.tvChucNangSD1 = (TextView) convertView.findViewById(R.id.tvChucNangSD1);
            viewHolder.tvTyLe = (TextView) convertView.findViewById(R.id.tvTyLe);
            viewHolder.tvDienTich = (TextView) convertView.findViewById(R.id.tvDTich);
            viewHolder.tvTyLe1 = (TextView) convertView.findViewById(R.id.tvTyLe1);
            viewHolder.tvDienTich1 = (TextView) convertView.findViewById(R.id.tvDienTich1);
            viewHolder.layoutQHItem = (LinearLayout) convertView.findViewById(R.id.layoutItemQH);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvSTT.setText(String.valueOf(position + 1));

        if (quyHoachItem.getMaOPho().equals("None")) {
            viewHolder.tvOPho.setVisibility(GONE);
            viewHolder.tvOPho1.setVisibility(GONE);
        } else {
            viewHolder.tvOPho.setVisibility(VISIBLE);
            viewHolder.tvOPho1.setVisibility(VISIBLE);
            viewHolder.tvOPho.setText(quyHoachItem.getMaOPho());
        }

        viewHolder.tvOPho.setText(quyHoachItem.getMaOPho());
        viewHolder.tvChucNangSD.setText(quyHoachItem.getChucNangSDD());
        viewHolder.tvDienTich.setText("Khoảng " + quyHoachItem.getDienTich());
        viewHolder.tvTyLe.setText(String.valueOf(quyHoachItem.getPhanTramDT() + "%"));
        viewHolder.tvChucNangSD1.setPadding(viewHolder.tvChucNangSD.getPaddingLeft(),
                viewHolder.tvChucNangSD.getPaddingTop(), viewHolder.tvChucNangSD.getPaddingRight(),
                viewHolder.tvChucNangSD.getPaddingBottom());
        viewHolder.tvChucNangSD1.setPadding(viewHolder.tvChucNangSD.getPaddingLeft(),
                viewHolder.tvChucNangSD.getPaddingTop(), viewHolder.tvChucNangSD.getPaddingRight(),
                viewHolder.tvChucNangSD.getPaddingBottom());

        String RGBColor = quyHoachItem.getRGBColor();
        String[] colors = RGBColor.split(",");
        viewHolder.layoutQHItem.setBackgroundColor(Color.rgb(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]),
                Integer.valueOf(colors[2])));
        if (RGBColor.equals("255,127,127") || RGBColor.equals("255,159,127") ||
                RGBColor.equals("255,191,0") || RGBColor.equals("0,255,0")) {
            viewHolder.tvSTT.setTextColor(Color.BLACK);
            viewHolder.tvOPho.setTextColor(Color.BLACK);
            viewHolder.tvChucNangSD.setTextColor(Color.BLACK);
            viewHolder.tvDienTich.setTextColor(Color.BLACK);
            viewHolder.tvTyLe.setTextColor(Color.BLACK);
            viewHolder.tvSTT1.setTextColor(Color.BLACK);
            viewHolder.tvOPho1.setTextColor(Color.BLACK);
            viewHolder.tvChucNangSD1.setTextColor(Color.BLACK);
            viewHolder.tvDienTich1.setTextColor(Color.BLACK);
            viewHolder.tvTyLe1.setTextColor(Color.BLACK);
        }
        else {
            viewHolder.tvSTT.setTextColor(Color.WHITE);
            viewHolder.tvOPho.setTextColor(Color.WHITE);
            viewHolder.tvChucNangSD.setTextColor(Color.WHITE);
            viewHolder.tvDienTich.setTextColor(Color.WHITE);
            viewHolder.tvTyLe.setTextColor(Color.WHITE);
            viewHolder.tvSTT1.setTextColor(Color.WHITE);
            viewHolder.tvOPho1.setTextColor(Color.WHITE);
            viewHolder.tvChucNangSD1.setTextColor(Color.WHITE);
            viewHolder.tvDienTich1.setTextColor(Color.WHITE);
            viewHolder.tvTyLe1.setTextColor(Color.WHITE);
        }
        return convertView;
    }
}