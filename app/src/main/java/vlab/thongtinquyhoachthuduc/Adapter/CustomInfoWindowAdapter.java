package vlab.thongtinquyhoachthuduc.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.HashMap;

import vlab.thongtinquyhoachthuduc.Model.QHInfo;
import vlab.thongtinquyhoachthuduc.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.markers;
import static vlab.thongtinquyhoachthuduc.Activity.MainActivity.markersOrder;

public class CustomInfoWindowAdapter implements MapboxMap.InfoWindowAdapter{
    private Context mContext;
    private View view;

    public CustomInfoWindowAdapter(Context context) {
        mContext = context;
        view = ((LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.infowindow, null, false);
    }

    @Nullable
    public View getInfoWindow(@NonNull Marker marker) {
        TextView tvSTT = (TextView) view.findViewById(R.id.tvSTT);
        TextView tvOPho = (TextView) view.findViewById(R.id.tvOPho);
        TextView tvSTT1 = (TextView) view.findViewById(R.id.tvSTT1);
        TextView tvOPho1 = (TextView) view.findViewById(R.id.tvOPho1);
        TextView tvChucNangSD = (TextView) view.findViewById(R.id.tvChucNangSD);
        TextView tvChucNangSD1 = (TextView) view.findViewById(R.id.tvChucNangSD1);
        TextView tvTyLe = (TextView) view.findViewById(R.id.tvTyLe);
        TextView tvDienTich = (TextView) view.findViewById(R.id.tvDTich);
        TextView tvTyLe1 = (TextView) view.findViewById(R.id.tvTyLe1);
        TextView tvDienTich1 = (TextView) view.findViewById(R.id.tvDienTich1);
        LinearLayout layoutQHItem = (LinearLayout) view.findViewById(R.id.layoutItemQH);

        QHInfo qhInfo = markers.get(marker.getId());

        if (qhInfo != null) {
            tvSTT.setText(String.valueOf(markersOrder.get(marker.getId())));

            if (qhInfo.getMaOPho().equals("None")) {
                tvOPho.setVisibility(GONE);
                tvOPho1.setVisibility(GONE);
            }
            else {
                tvOPho.setVisibility(VISIBLE);
                tvOPho1.setVisibility(VISIBLE);
                tvOPho.setText(qhInfo.getMaOPho());
            }

            tvChucNangSD.setText(qhInfo.getChucNangSDD());
            tvDienTich.setText("Khoảng " + qhInfo.getDienTich());
            tvTyLe.setText(String.valueOf(qhInfo.getPhanTramDT() + "%"));

            String RGBColor = qhInfo.getRGBColor();
            String[] colors = RGBColor.split(",");
            layoutQHItem.setBackgroundColor(Color.rgb(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]),
                    Integer.valueOf(colors[2])));

            if (RGBColor.equals("255,127,127") || RGBColor.equals("255,159,127") ||
                    RGBColor.equals("255,191,0") || RGBColor.equals("0,255,0")) {
                tvSTT.setTextColor(Color.BLACK);
                tvOPho.setTextColor(Color.BLACK);
                tvChucNangSD.setTextColor(Color.BLACK);
                tvDienTich.setTextColor(Color.BLACK);
                tvTyLe.setTextColor(Color.BLACK);
                tvSTT1.setTextColor(Color.BLACK);
                tvOPho1.setTextColor(Color.BLACK);
                tvChucNangSD1.setTextColor(Color.BLACK);
                tvDienTich1.setTextColor(Color.BLACK);
                tvTyLe1.setTextColor(Color.BLACK);
            } else {
                tvSTT.setTextColor(Color.WHITE);
                tvOPho.setTextColor(Color.WHITE);
                tvChucNangSD.setTextColor(Color.WHITE);
                tvDienTich.setTextColor(Color.WHITE);
                tvTyLe.setTextColor(Color.WHITE);
                tvSTT1.setTextColor(Color.WHITE);
                tvOPho1.setTextColor(Color.WHITE);
                tvChucNangSD1.setTextColor(Color.WHITE);
                tvDienTich1.setTextColor(Color.WHITE);
                tvTyLe1.setTextColor(Color.WHITE);
            }
        }
        return view;
    }
}
