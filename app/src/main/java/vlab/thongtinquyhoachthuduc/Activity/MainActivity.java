package vlab.thongtinquyhoachthuduc.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerView;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationSource;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.style.layers.CannotAddLayerException;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.CannotAddSourceException;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import com.mapbox.services.android.telemetry.location.LocationEngine;
import com.mapbox.services.android.telemetry.location.LocationEngineListener;
import com.mapbox.services.android.telemetry.permissions.PermissionsManager;
import com.mapbox.services.commons.geojson.Feature;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import vlab.thongtinquyhoachthuduc.Adapter.CustomInfoWindowAdapter;
import vlab.thongtinquyhoachthuduc.Adapter.QuyHoachListAdapter;
import vlab.thongtinquyhoachthuduc.Model.QHChiTiet;
import vlab.thongtinquyhoachthuduc.Model.QHInfo;
import vlab.thongtinquyhoachthuduc.Model.ThuaDatInfo;
import vlab.thongtinquyhoachthuduc.R;
import vlab.thongtinquyhoachthuduc.Utils.BitmapHelper;
import vlab.thongtinquyhoachthuduc.Utils.DeviceSizeHelper;
import vlab.thongtinquyhoachthuduc.Utils.DialogHelper;
import vlab.thongtinquyhoachthuduc.Utils.FileHelper;
import vlab.thongtinquyhoachthuduc.Utils.LocationHelper;
import vlab.thongtinquyhoachthuduc.Utils.VolleySingleton;
import vlab.thongtinquyhoachthuduc.CustomDialog.*;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mapbox.mapboxsdk.maps.MapView.DID_FAIL_LOADING_MAP;
import static com.mapbox.mapboxsdk.maps.MapView.DID_FINISH_LOADING_MAP;
import static vlab.thongtinquyhoachthuduc.Utils.DetailLandHelper.addGlossary;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        View.OnClickListener {
    // Layout variables
    private FloatingActionButton btnLocation;
    public static FloatingActionButton btnHelp;
    public static LinearLayout buttonSearch, mapType;
    public static CoordinatorLayout layoutMain;
    private LinearLayout layoutLuuY, layoutSearchThua, layoutXemThongTinThua, panelThaoTac, layoutQHCT;
    private ImageView btnClose;
    private Spinner spMapType, spTown;
    private TextView tvTP, tvQuan, tvPhuong, tvSoThua, tvSoTo,
            tvDienTich, tvQHPhanKhu, tvQHChiTiet, tvQHChiTiet1, btnXemQH,
            btnXuatThongTin, btnTimMoi; // textview of layoutXemThongTinThua
    private TextView tvTenDuAn, tvQuanQHCT, tvPhuongQHCT, tvDienTichQHCT, tvTrangThai, tvCoQuanPheDuyet,
    tvSoQuyetDinh, tvHeaderThuaDat, tvHeaderQHCT; //textview of layoutQHCT
    private TextView tvQHPK, tvQHCT;
    private LinearLayout btnQHPK, btnQHCT;
    private Button btnSearch;
    private EditText edtSoThua, edtSoTo;
    private View bottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private AlertDialog diag;
    private ExpandableHeightListView QHListView;
    public static PopupWindow popupWindowHelper;
    private AVLoadingIndicatorView loadingIndicator;
    private int height;
    private MapView mapView;
    private MapboxMap mMap;

    // Global variables
    private Gson gson;
    private String maThuaDat;
    private ArrayList<QHInfo> QHInfos;
    private QuyHoachListAdapter QHListAdapter;
    private boolean isBtnXemQHClicked = false, isBtnSearchClicked = false, isBottomSheetExpanding = false,
    isXemQHCT = false;
    public static HashMap<Long, QHInfo> markers;
    public static HashMap<Long, Integer> markersOrder;
    private ArrayList<Marker> markersArray;
    private String phuong;
    private long period = 2000;
    private long backPressedTime;
    public static int dialogNumber = 1;
    public static boolean isHelpFinished = false;
    private SharedPreferences locationPermissionStatus, storagePermissionStatus, notShowStatePref;
    private boolean isOff = false;
    private boolean userIsInteracting = false;
    private ThuaDatInfo thuaDatInfo;
    private QHChiTiet QHCTInfo;

    // Layer variables
    private Layer thuadat;
    private Source thuadatSource;
    private JSONArray coordinatesThuaDat;
    private Layer thuaDatStroke;
    private Layer ranhQHCTStroke;
    private Source ranhQHCTSource;
    private ArrayList<Layer> sddLayerList;
    private ArrayList<Layer> sddStrokeList;
    private ArrayList<Source> sddSourceList;

    // Location variables
   // private LocationServices locationServices;
    private LocationEngine locationEngine;
    private LocationEngineListener locationEngineListener;
    private boolean initialLocationSet = false;
    private MarkerView userMarker;

    // Save instance state variables
    private JSONObject jsonThuaDat;
    private JSONArray jsonSdd;
    private JSONObject jsonQHCT;

    // Permission request variables
    private static final int PERMISSIONS_LOCATION = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int PERMISSION_CALLBACK_CONSTANT = 102;
    private String[] permissionsRequired;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //MapboxAccountManager.start(getApplicationContext(), getString(R.string.access_token));
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            DialogHelper.showErrorDialog(this,"Thiết bị không được hỗ trợ", "Ứng dụng chỉ hỗ trợ hệ điều hành Android 4.4 trở lên.");
            finish();
        } else {
            locationEngine = LocationSource.getLocationEngine(this);
            mapView = (MapView) findViewById(R.id.mapview);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);
            addViews();
            addEvents();
            isBtnXemQHClicked = isBtnSearchClicked = false;
            notShowStatePref = getSharedPreferences("notShowAgain", MODE_PRIVATE);
            locationPermissionStatus = getSharedPreferences("locationPermission", MODE_PRIVATE);
            storagePermissionStatus = getSharedPreferences("storagePermission", MODE_PRIVATE);

            if (savedInstanceState != null) { // Get saved json thua dat and sdd dat
                checkSavedInstanceState(savedInstanceState);
            }
            else {
                if (!notShowStatePref.getBoolean("notShowAgain", false)) {
                    MainHelpDialog mainHelpDialog = new MainHelpDialog(this);
                    mainHelpDialog.show();
                }
            }
        }
    }

    private void checkHelpDialog(Bundle savedInstanceState) {
        if (!notShowStatePref.getBoolean("notShowAgain", false)) {

            if (savedInstanceState.containsKey("isHelpFinished")) {
                int number = savedInstanceState.getInt("dialogNumber");
                if (number == 1) {
                    MainHelpDialog mainHelpDialog = new MainHelpDialog(MainActivity.this);
                    mainHelpDialog.show();
                }

                if (number == 2) {
                    popupWindowHelper = new PopupWindow(MainActivity.this);
                    popupWindowHelper.displaySearchHelpPopup();
                }

                if (number == 3) {
                    popupWindowHelper = new PopupWindow(MainActivity.this);
                    popupWindowHelper.displayMapTypeHelpPopup();
                }

                if (number == 4) {
                    popupWindowHelper = new PopupWindow(MainActivity.this);
                    popupWindowHelper.displayGlossaryHelpPopup();
                }

                if (number == 5) {
                    EndHelpDialog endHelpDialog = new EndHelpDialog(MainActivity.this);
                    endHelpDialog.show();
                }
            }
        }
    }

    private void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey("jsonThuaDat")) {
            try {
                jsonThuaDat = new JSONObject(savedInstanceState.getString("jsonThuaDat"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (savedInstanceState.containsKey("jsonSdd")) { // Get saved json quy hoach sdd dat
            try {
                jsonSdd = new JSONArray(savedInstanceState.getString("jsonSdd"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (savedInstanceState.containsKey("jsonQHCT")) {
            try {
                jsonQHCT = new JSONObject(savedInstanceState.getString("jsonQHCT"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (savedInstanceState.containsKey("isBtnSearchClicked")) {
            isBtnSearchClicked = savedInstanceState.getBoolean("isBtnSearchClicked");
        }

        if (savedInstanceState.containsKey("isBtnXemQHClicked")) {
            isBtnXemQHClicked = savedInstanceState.getBoolean("isBtnXemQHClicked");
        }

        if (savedInstanceState.containsKey("isXemQHCT")) {
            isXemQHCT = savedInstanceState.getBoolean("isXemQHCT");
        }

        if (savedInstanceState.containsKey("isDiagHelpShowing")) {
            diag.show();
        }

        if (savedInstanceState.containsKey("isBottomSheetExpanding")) {
            isBottomSheetExpanding = true;
        }
        checkHelpDialog(savedInstanceState);
    }

    private void createSavedInstance() {
        if (jsonThuaDat != null) {
            List<ThuaDatInfo> thuadatList = Arrays.asList(gson.fromJson("[" + jsonThuaDat.toString() + "]",
                    ThuaDatInfo[].class));
            thuaDatInfo = thuadatList.get(0);   // parse json to class ThuaDatInfo
            layoutSearchThua.setVisibility(GONE);
            layoutXemThongTinThua.setVisibility(VISIBLE);
            setUpLayoutXemThongTinThua(thuaDatInfo);
            maThuaDat = thuaDatInfo.getMaThuaDat();

            if (jsonSdd != null) { // Check for JSONArray of quy hoach sdd dat is displayed before or not
                for (int i = 0; i < jsonSdd.length(); i++) {
                    try {
                        String json = "[" + jsonSdd.getJSONObject(i).toString() + "]";
                        List<QHInfo> QHList = Arrays.asList(gson.fromJson(json, QHInfo[].class));
                        QHListAdapter.add(QHList.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                QHListAdapter.notifyDataSetChanged();
                markers = new HashMap<>();
                markersOrder = new HashMap<>();
                markersArray = new ArrayList<>();
                sddLayerList = new ArrayList<>();
                sddStrokeList = new ArrayList<>();
                sddSourceList = new ArrayList<>();
                setUpQHCT();
            }
            else {
                setUpQHCT();
            }
        }
    }

    private void setUpQHCT() {
        if (jsonQHCT != null) {
            btnQHCT.setVisibility(VISIBLE);
            btnQHPK.setVisibility(VISIBLE);

            if (isXemQHCT) {
                btnQHPK.setBackgroundResource(R.drawable.button_normal);
                tvQHPK.setTextColor(Color.BLACK);
                btnQHCT.setBackgroundResource(R.drawable.button2_selected);
                tvQHCT.setTextColor(Color.WHITE);
                tvHeaderThuaDat.setVisibility(VISIBLE);
                tvHeaderQHCT.setVisibility(VISIBLE);
                panelThaoTac.setVisibility(GONE);
                QHListView.setVisibility(GONE);
                layoutLuuY.setVisibility(GONE);
                layoutQHCT.setVisibility(VISIBLE);
                List<QHChiTiet> QHCTList = Arrays.asList(gson.fromJson("[" + jsonQHCT.toString() + "]",
                        QHChiTiet[].class));
                QHCTInfo = QHCTList.get(0);
                setUpLayoutQHCT(QHCTInfo);
            } else {
                btnQHPK.setBackgroundResource(R.drawable.button2_selected);
                tvQHPK.setTextColor(Color.WHITE);
                btnQHCT.setBackgroundResource(R.drawable.button_normal);
                tvQHCT.setTextColor(Color.BLACK);
            }
        }
    }

    private void addViews() {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.layout_main);
        layoutMain = (CoordinatorLayout) findViewById(R.id.layout_main);
        mapType = (LinearLayout) findViewById(R.id.layout_maptype);

        layoutSearchThua = (LinearLayout) findViewById(R.id.layoutSearchSoThua);
        layoutXemThongTinThua = (LinearLayout) findViewById(R.id.layoutXemThongTin);
        layoutQHCT = (LinearLayout) findViewById(R.id.layoutQHCT);
        layoutLuuY = (LinearLayout) findViewById(R.id.layoutLuuY);

        spMapType = (Spinner) findViewById(R.id.spinnerMapType);
        spTown = (Spinner) findViewById(R.id.spinnerTown);

        btnQHPK = (LinearLayout) findViewById(R.id.btnQHPK);
        btnQHCT = (LinearLayout) findViewById(R.id.btnQHCT);
        tvQHPK = (TextView) findViewById(R.id.tvQHPK);
        tvQHCT = (TextView) findViewById(R.id.tvQHCT);
        btnLocation = (FloatingActionButton) findViewById(R.id.buttonLocation);
        btnHelp = (FloatingActionButton) findViewById(R.id.buttonHelp);
        buttonSearch = (LinearLayout) findViewById(R.id.buttonSearch);

        tvHeaderThuaDat = (TextView) findViewById(R.id.headerThuaDatInfo);
        tvHeaderQHCT = (TextView) findViewById(R.id.headerQHCT);
        panelThaoTac = (LinearLayout) findViewById(R.id.panelThaoTac);
        btnClose = (ImageView) findViewById(R.id.btnClose);
        btnXemQH = (TextView) layoutXemThongTinThua.findViewById(R.id.btnXemQH);
        btnXuatThongTin = (TextView)  layoutXemThongTinThua.findViewById(R.id.btnXuatThongTin);

        bottomSheet = coordinatorLayout.findViewById(R.id.searchLayout);
        ViewGroup.LayoutParams params = bottomSheet.getLayoutParams();
        height = DeviceSizeHelper.getHeightScreen(this);

        if (DeviceSizeHelper.getRotation(getApplicationContext()) == 1) {
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) btnHelp.getLayoutParams();
            lp.setAnchorId(View.NO_ID);
            lp.gravity = Gravity.BOTTOM | GravityCompat.END;
            height -= DeviceSizeHelper.getStatusBarHeight(this);
            params.height = height;
        } else {
            height = height * 1 / 2;
            params.height = height;
        }

        bottomSheet.setLayoutParams(params);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        QHListView = (ExpandableHeightListView) layoutXemThongTinThua.findViewById(R.id.listQuyHoach);
        btnTimMoi = (TextView) layoutXemThongTinThua.findViewById(R.id.btnTimMoi);
        btnSearch = (Button) findViewById(R.id.btnSearchThua);
        edtSoThua = (EditText) findViewById(R.id.edtSoThua);
        edtSoTo = (EditText) findViewById(R.id.edtSoTo);

        tvTP = (TextView) layoutXemThongTinThua.findViewById(R.id.tvTP);
        tvQuan = (TextView) layoutXemThongTinThua.findViewById(R.id.tvQuan);
        tvPhuong = (TextView) layoutXemThongTinThua.findViewById(R.id.tvPhuong);
        tvSoThua = (TextView) layoutXemThongTinThua.findViewById(R.id.tvSoThua);
        tvSoTo = (TextView) layoutXemThongTinThua.findViewById(R.id.tvSoTo);
        tvDienTich = (TextView) layoutXemThongTinThua.findViewById(R.id.tvDienTich);
        tvQHPhanKhu = (TextView) layoutXemThongTinThua.findViewById(R.id.tvQHPhanKhu);
        tvQHChiTiet = (TextView) layoutXemThongTinThua.findViewById(R.id.tvQHChiTiet);
        tvQHChiTiet1 = (TextView) layoutXemThongTinThua.findViewById(R.id.tvQHChiTiet1);

        tvTenDuAn = (TextView) findViewById(R.id.tvTenDuAn);
        tvQuanQHCT = (TextView) findViewById(R.id.tvQuanQHCT);
        tvPhuongQHCT = (TextView) findViewById(R.id.tvPhuongQHCT);
        tvDienTichQHCT = (TextView) findViewById(R.id.tvDienTichQHCT);
        tvTrangThai = (TextView) findViewById(R.id.tvTrangThai);
        tvCoQuanPheDuyet = (TextView) findViewById(R.id.tvCoQuanPheDuyet);
        tvSoQuyetDinh = (TextView) findViewById(R.id.tvSoQuyetDinh);

        loadingIndicator = (AVLoadingIndicatorView) findViewById(R.id.loading_indicator);
        diag = addGlossary(MainActivity.this);

        if (isBottomSheetExpanding) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private void addEvents() {
        GsonBuilder gsonBuilder = new GsonBuilder(); // build gson for json parse to class
        gson = gsonBuilder.create();

        QHInfos = new ArrayList<>(); // build lisview for QH chi tiet info
        QHListAdapter = new QuyHoachListAdapter(this, QHInfos);
        QHListView.setAdapter(QHListAdapter);
        QHListView.setExpanded(true);

        spMapType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        if (mMap != null) {
                            removeLayer();
                            mMap.setStyleUrl(getString(R.string.nen_thua_dat));
                        }
                        break;
                        case 1:
                        if (mMap != null) {
                            removeLayer();
                            mMap.setStyleUrl(getString(R.string.nen_ban_do));
                        }
                        break;
                    case 2:
                        if (mMap != null) {
                            removeLayer();
                            mMap.setStyleUrl(getString(R.string.nen_khong_anh));
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() { // behavior of panel search
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                buttonSearch.setVisibility(VISIBLE);

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    if (coordinatesThuaDat != null) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat), 250));
                    }
                }
                else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    buttonSearch.setVisibility(GONE);

                    if (coordinatesThuaDat != null) {
                        if (DeviceSizeHelper.getRotation(getApplicationContext()) == 0) {
                           mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                                   250, 250, 250, bottomSheet.getHeight()));
                    } else if (DeviceSizeHelper.getRotation(getApplicationContext()) == 1) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                                    bottomSheet.getWidth(), 250, 250, 250));
                        }
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
        });

        spTown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // droplist Phường
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phuong = spTown.getSelectedItem().toString();
                loadingIndicator.smoothToShow();
                LatLng location;
                if (phuong.equals("Linh Chiểu"))
                    location = new LatLng(10.8538209, 106.7617984);
                else if (phuong.equals("Bình Thọ"))
                    location = new LatLng(10.8467644, 106.7662221);
                else location = new LatLng(10.8560516, 106.7535475);

                if (mMap != null && userIsInteracting) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng((location).getLatitude(),
                            (location).getLongitude()), 15));
                }

                loadingIndicator.smoothToHide();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        btnQHPK.setOnClickListener(this);
        btnQHCT.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnXemQH.setOnClickListener(this);
        btnXuatThongTin.setOnClickListener(this);
        btnTimMoi.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);
    }

    private void removeLayer() {
        mMap.clear();

        if (markersArray != null) {
            for (Marker marker : markersArray) {
                mMap.removeMarker(marker);
            }
            markersArray.clear();
        }

        if (thuadat != null && thuaDatStroke != null) {
            try {
                if (mMap.getLayer(thuadat.getId()) != null) {
                    mMap.removeLayer(thuadat);
                }

                if (mMap.getLayer(thuaDatStroke.getId()) != null) {
                    mMap.removeLayer(thuaDatStroke);
                }
                thuadat = null;
                thuaDatStroke = null;
           } catch (CannotAddLayerException e) {
                e.printStackTrace();
            }

           try {
                if (thuadatSource != null && mMap.getSource(thuadatSource.getId()) != null) {
                    mMap.removeSource(thuadatSource);
                    thuadatSource = null;
                }
           } catch (CannotAddLayerException e) {
                e.printStackTrace();
            }
        }

        if (sddLayerList != null && sddSourceList != null && sddStrokeList != null) {
            for (Layer layer : sddLayerList) {
               try {
                    if (mMap.getLayer(layer.getId()) != null) {
                        mMap.removeLayer(layer);
                    }
               } catch (CannotAddLayerException e) {
                    e.printStackTrace();
                }
            }
            sddLayerList.clear();

            for (Layer sddStroke : sddStrokeList) {
              try {
                    if (mMap.getLayer(sddStroke.getId()) != null) {
                        mMap.removeLayer(sddStroke);
                    }
              } catch (CannotAddLayerException e) {
                    e.printStackTrace();
                }
            }
            sddStrokeList.clear();

            for (Source source : sddSourceList) {
                try {
                    if (mMap.getSource(source.getId()) != null) {
                        mMap.removeSource(source);
                    }
                } catch (CannotAddSourceException e) {
                    e.printStackTrace();
                }
            }
            sddSourceList.clear();
        }

        if (ranhQHCTSource != null && ranhQHCTStroke != null) {
            try {
                if (mMap.getLayer(ranhQHCTStroke.getId()) != null) {
                    mMap.removeLayer(ranhQHCTStroke);
                    ranhQHCTStroke = null;
                }
            } catch (CannotAddLayerException e) {
                e.printStackTrace();
            }

            try {
                if (mMap.getSource(ranhQHCTSource.getId()) != null) {
                    mMap.removeSource(ranhQHCTSource);
                    ranhQHCTSource = null;
                }
            } catch (CannotAddSourceException e) {
                e.printStackTrace();
            }
        }
    }

    private void addLayer() {
        if (jsonSdd != null) {
            parseGeoSDDThuaDat(jsonSdd);
        } else if (jsonThuaDat != null) {
            parseGeoThuaDat(jsonThuaDat);
        }

        if (jsonQHCT != null && isXemQHCT) {
            drawRanhQHCT();
        }
    }

    @Override
    public void onMapReady(final MapboxMap mapboxMap) {
        mMap = mapboxMap;
        mMap.setMinZoomPreference(13);
        mMap.setStyleUrl(getString(R.string.nen_thua_dat));
       // mMap.setMinZoom(13);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setAttributionEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setCompassMargins(0, 150, 0, 0);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.8538209, 106.7617984), 15));

        mapView.addOnMapChangedListener(new MapView.OnMapChangedListener() {
            @Override
            public void onMapChanged(int change) {
                if (change == DID_FAIL_LOADING_MAP) {
                    int pos = spMapType.getSelectedItemPosition();

                    switch (pos) {
                        case 0:
                            mMap.setStyleUrl(getString(R.string.nen_thua_dat));
                            break;
                        case 1:
                            mMap.setStyleUrl(getString(R.string.nen_ban_do));
                            break;
                        case 2:
                            mMap.setStyleUrl(getString(R.string.nen_khong_anh));
                            break;
                    }
                }

                if (change == DID_FINISH_LOADING_MAP) {
                    addLayer();
                }
            }
        });

        mMap.setOnMapClickListener(new MapboxMap.OnMapClickListener() { // detect which thua dat is touched
            @Override
            public void onMapClick(@NonNull LatLng point) {
                final PointF pixel = mMap.getProjection().toScreenLocation(point); // get map pixel touched
                List<Feature> features = mMap.queryRenderedFeatures(pixel); // get features from that pixel

                if (features.size() > 1) {
                    for (int i = 0; i < features.size(); i++) {
                        if (features.get(i) != null && features.get(i).getProperties().has("MaThuaDat")) { // get value of key MaThuaDat
                            isBtnXemQHClicked = false; // prepare to load thua dat touched
                            isBtnSearchClicked = true;
                            clearElements();
                            QHListAdapter.clear();
                            QHListAdapter.notifyDataSetChanged();
                            QHListView.setVisibility(GONE);
                            layoutLuuY.setVisibility(GONE);

                            String MaThuaDat = features.get(i).getStringProperty("MaThuaDat");
                            getJsonThuaDat(getString(R.string.get_thuadat_info) + MaThuaDat);

                            if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            }

                            loadingIndicator.smoothToShow();
                            maThuaDat = MaThuaDat;
                            jsonSdd = null;
                            break;
                        }
                    }
                }
            }
        });

        btnLocation.setOnClickListener(this);
        createSavedInstance();

        if (jsonThuaDat != null) { // create camera view for thua dat
            try {
                coordinatesThuaDat = LocationHelper.parseCoordinates(jsonThuaDat.getString("Ranh")).getJSONArray(0);
                if (DeviceSizeHelper.getRotation(getApplicationContext()) == 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                            250, 250, 250, bottomSheet.getHeight()));
                } else if (DeviceSizeHelper.getRotation(getApplicationContext()) == 1) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                            bottomSheet.getWidth(), 250, 250, 250));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSearchThua:
                if (!edtSoTo.getText().toString().equals("") && !edtSoThua.getText().toString().equals("")) {

                    if (!isBtnSearchClicked) {
                        loadingIndicator.smoothToShow();
                        String maPhuong, URL, soTo, soThua;
                        URL = getString(R.string.get_thuadat_info);

                        if (phuong.equals("Bình Thọ")) maPhuong = "26824";
                        else if (phuong.equals("Linh Chiểu")) maPhuong = "26815";
                        else maPhuong = "26818";
                        soTo = String.format("%03d", Integer.parseInt(edtSoTo.getText().toString()));
                        soThua = String.format("%04d", Integer.parseInt(edtSoThua.getText().toString()));
                        URL += maPhuong + soTo + soThua;
                        maThuaDat = maPhuong + soTo + soThua;
                        getJsonThuaDat(URL);
                        QHListView.setVisibility(GONE);
                        layoutLuuY.setVisibility(GONE);
                    }
                }
                else {
                    DialogHelper.showErrorDialog(MainActivity.this, "Lỗi", "Hãy nhập số thửa và số tờ.");
                }
                break;

            case R.id.buttonSearch:
                if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;

            case R.id.btnClose:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.btnXemQH:
                String URL = getString(R.string.get_sdd_thuadat);
                if (maThuaDat != null && !isBtnXemQHClicked) {
                    loadingIndicator.smoothToShow();
                    URL += maThuaDat;
                    layoutLuuY.setVisibility(VISIBLE);
                    QHListView.setVisibility(VISIBLE);
                    getJsonQuyHoachSDD(URL);
                    QHListAdapter.clear();
                    QHListAdapter.notifyDataSetChanged();
                }
                break;

            case R.id.btnXuatThongTin: // push link to DownloadManager
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    permissionsRequired = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    checkStoragePermission();
                } else {
                    FileHelper.saveFile(this, maThuaDat, thuaDatInfo);
                }

                break;

            case R.id.btnTimMoi: // reset panel search
                layoutXemThongTinThua.setVisibility(GONE);
                layoutSearchThua.setVisibility(VISIBLE);
                edtSoThua.setText("");
                edtSoTo.setText("");
                spTown.setSelection(0);
                isBtnXemQHClicked = false;
                isBtnSearchClicked = false;
                isXemQHCT = false;
                btnQHCT.setVisibility(GONE);
                btnQHPK.setVisibility(GONE);
                clearElements();
                break;

            case R.id.buttonHelp:
                diag.show();
                break;

            case R.id.buttonLocation:
                if (mMap != null) {
                  //locationServices = LocationServices.getLocationServices(MainActivity.this);
                    toggleGps(!mMap.isMyLocationEnabled());
                }
                break;
            case R.id.btnQHPK:
                tvHeaderThuaDat.setVisibility(GONE);
                tvHeaderQHCT.setVisibility(GONE);
                panelThaoTac.setVisibility(VISIBLE);
                QHListView.setVisibility(VISIBLE);
                layoutLuuY.setVisibility(VISIBLE);
                layoutQHCT.setVisibility(GONE);
                btnQHCT.setBackgroundResource(R.drawable.button_normal);
                tvQHCT.setTextColor(Color.BLACK);
                btnQHPK.setBackgroundResource(R.drawable.button2_selected);
                tvQHPK.setTextColor(Color.WHITE);
                tvQHChiTiet.setVisibility(VISIBLE);
                tvQHChiTiet1.setVisibility(VISIBLE);
                isXemQHCT = false;

                if (ranhQHCTSource != null && ranhQHCTStroke != null) {
                    try {
                        if (mMap.getLayer(ranhQHCTStroke.getId()) != null) {
                            mMap.removeLayer(ranhQHCTStroke);
                            ranhQHCTStroke = null;

                        }
                    } catch (CannotAddLayerException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (mMap.getSource(ranhQHCTSource.getId()) != null) {
                            mMap.removeSource(ranhQHCTSource);
                            ranhQHCTSource = null;
                        }
                    } catch (CannotAddSourceException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tvQHChiTiet:
                loadingIndicator.smoothToShow();
                getJsonQHCT(getString(R.string.get_qh_chitiet) + maThuaDat);
                break;
            case R.id.btnQHCT:
                tvHeaderThuaDat.setVisibility(VISIBLE);
                tvHeaderQHCT.setVisibility(VISIBLE);
                panelThaoTac.setVisibility(GONE);
                QHListView.setVisibility(GONE);
                layoutLuuY.setVisibility(GONE);
                layoutQHCT.setVisibility(VISIBLE);
                btnQHCT.setBackgroundResource(R.drawable.button2_selected);
                tvQHCT.setTextColor(Color.WHITE);
                btnQHPK.setBackgroundResource(R.drawable.button_normal);
                tvQHPK.setTextColor(Color.BLACK);
                tvQHChiTiet.setVisibility(GONE);
                tvQHChiTiet1.setVisibility(GONE);
                isXemQHCT = true;
                drawRanhQHCT();
                break;
        }
    }

    private void checkStoragePermission() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[0])
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(MainActivity.this,
                permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[1])) {
                showStorageRequestDialog1(MainActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            } else if (storagePermissionStatus.getBoolean(permissionsRequired[0], false)) {
                showStorageRequestDialog();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }
            SharedPreferences.Editor editor = storagePermissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            FileHelper.saveFile(this, maThuaDat, thuaDatInfo);
        }
    }

    private void clearElements() { // clear left elements when click btnTimMoi
        mMap.clear();

       if (thuadat != null && thuaDatStroke != null) {
           try {
               if (mMap.getLayer(thuadat.getId()) != null) {
                   mMap.removeLayer(thuadat);
                   thuadat = null;
               }

               if (mMap.getLayer(thuaDatStroke.getId()) != null) {
                   mMap.removeLayer(thuaDatStroke);
                   thuaDatStroke = null;
               }
           } catch (CannotAddLayerException e) {
               e.printStackTrace();
           }

           if (thuadatSource != null) {
               try {
                   if (mMap.getSource(thuadatSource.getId()) != null) {
                       mMap.removeSource(thuadatSource);
                       thuadatSource = null;
                   }
               } catch (CannotAddSourceException e) {
                   e.printStackTrace();
               }
           }
       }

        if (sddLayerList != null && sddSourceList != null && sddStrokeList != null) {
            for (Layer layer : sddLayerList) {
                try {
                    if (mMap.getLayer(layer.getId()) != null) {
                        mMap.removeLayer(layer);
                    }
                } catch (CannotAddLayerException e) {
                    e.printStackTrace();
                }
            }
            sddLayerList.clear();

            for (Layer sddStroke : sddStrokeList) {
                try {
                    if (mMap.getLayer(sddStroke.getId()) != null) {
                        mMap.removeLayer(sddStroke);
                    }
                } catch (CannotAddLayerException e) {
                    e.printStackTrace();
                }
            }
            sddStrokeList.clear();

            for (Source source : sddSourceList) {
                try {
                    if (mMap.getSource(source.getId()) != null) {
                        mMap.removeSource(source);
                    }
                } catch (CannotAddSourceException e) {
                    e.printStackTrace();
                }
            }
            sddSourceList.clear();
        }

        if (ranhQHCTSource != null && ranhQHCTStroke != null) {
           try {
                if (mMap.getLayer(ranhQHCTStroke.getId()) != null) {
                    mMap.removeLayer(ranhQHCTStroke);
                    ranhQHCTStroke = null;
                }
            } catch (CannotAddLayerException e) {
                e.printStackTrace();
            }

            try {
                if (mMap.getSource(ranhQHCTSource.getId()) != null) {
                    mMap.removeSource(ranhQHCTSource);
                    ranhQHCTSource = null;
                }
            } catch (CannotAddSourceException e) {
                e.printStackTrace();
            }
        }

        if (coordinatesThuaDat != null) {
            coordinatesThuaDat = null;
        }

        if (jsonThuaDat != null) {
            jsonThuaDat = null;
        }

        if (jsonSdd != null) {
            jsonSdd = null;
        }

        if (jsonQHCT != null) {
            jsonQHCT = null;
        }
    }

    private void getJsonThuaDat(String URL) {
        StringRequest str = new StringRequest(Request.Method.GET,
                URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null && !response.equals("[]")) {
                    List<ThuaDatInfo> thuadatList = Arrays.asList(gson.fromJson(response, ThuaDatInfo[].class)); // parse json to class ThuaDatInfo
                    thuaDatInfo = thuadatList.get(0);
                    layoutSearchThua.setVisibility(GONE);
                    layoutXemThongTinThua.setVisibility(VISIBLE);
                    btnQHCT.setVisibility(GONE);
                    btnQHPK.setVisibility(GONE);
                    layoutQHCT.setVisibility(GONE);
                    tvHeaderQHCT.setVisibility(GONE);
                    tvHeaderThuaDat.setVisibility(GONE);
                    panelThaoTac.setVisibility(VISIBLE);
                    setUpLayoutXemThongTinThua(thuaDatInfo);

                    try {
                        jsonThuaDat = new JSONArray(response).getJSONObject(0);
                        parseGeoThuaDat(jsonThuaDat);
                        isBtnSearchClicked = true;
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    DialogHelper.showErrorDialog(MainActivity.this, "Thông báo", "Thông tin thửa đất không thuộc vùng quy hoạch của hệ thống quản lý.");
                    isBtnSearchClicked = false;
                    loadingIndicator.smoothToHide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isBtnSearchClicked = false;
                loadingIndicator.smoothToHide();
                Toast.makeText(getApplicationContext(), "Không có kết nối đến server. Hãy thử lại!", Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue().add(str);
    }

    private void getJsonQHCT(String URL) {
        StringRequest str = new StringRequest(Request.Method.GET,
                URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null && !response.equals("[]")) {
                    List<QHChiTiet> QHCTList = Arrays.asList(gson.fromJson(response, QHChiTiet[].class)); // parse json to class ThuaDatInfo
                    QHCTInfo = QHCTList.get(0);
                    btnQHCT.setVisibility(VISIBLE);
                    btnQHPK.setVisibility(VISIBLE);
                    btnQHPK.setBackgroundResource(R.drawable.button_normal);
                    tvQHPK.setTextColor(Color.BLACK);
                    btnQHCT.setBackgroundResource(R.drawable.button2_selected);
                    tvQHCT.setTextColor(Color.WHITE);
                    tvHeaderThuaDat.setVisibility(VISIBLE);
                    tvHeaderQHCT.setVisibility(VISIBLE);
                    panelThaoTac.setVisibility(GONE);
                    QHListView.setVisibility(GONE);
                    layoutLuuY.setVisibility(GONE);
                    layoutQHCT.setVisibility(VISIBLE);
                    setUpLayoutQHCT(QHCTInfo);
                    isXemQHCT = true;

                    try {
                        jsonQHCT = new JSONArray(response).getJSONObject(0);
                        drawRanhQHCT();
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    DialogHelper.showErrorDialog(MainActivity.this, "Thông báo", "Thông tin thửa đất không thuộc vùng quy hoạch của hệ thống quản lý.");
                    loadingIndicator.smoothToHide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingIndicator.smoothToHide();
                isXemQHCT = false;
                Toast.makeText(getApplicationContext(), "Không có kết nối đến server. Hãy thử lại!", Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue().add(str);
    }

    private void drawRanhQHCT() {
        try {
            JSONArray featureList = new JSONArray();
            JSONObject result = new JSONObject();
            JSONObject point = new JSONObject(); // geometry feature
            point.put("type", "Polygon");
            point.put("coordinates", LocationHelper.parseCoordinates(jsonQHCT.getString("RanhQHCT")));
            JSONObject properties = new JSONObject(); // properties feature
            JSONObject feature = new JSONObject();
            feature.put("type", "Feature");
            feature.put("geometry", point);
            feature.put("properties", properties);
            featureList.put(feature);
            result.put("type", "FeatureCollection");
            result.put("features", featureList);

            ranhQHCTSource = new GeoJsonSource("ranhQHCT-src", result.toString());
            mMap.addSource(ranhQHCTSource);
            ranhQHCTStroke = new LineLayer("ranhQHCT-layer", "ranhQHCT-src");
            ranhQHCTStroke.setProperties(
                    PropertyFactory.lineWidth(3f),
                    PropertyFactory.lineColor(Color.rgb(255, 0, 0))
            );
            mMap.addLayer(ranhQHCTStroke);
            JSONArray ranh = LocationHelper.parseCoordinates(jsonQHCT.getString("RanhQHCT")).getJSONArray(0);

            if (DeviceSizeHelper.getRotation(getApplicationContext()) == 0) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(ranh),
                        250, 250, 250, bottomSheet.getHeight()));
            } else if (DeviceSizeHelper.getRotation(getApplicationContext()) == 1) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(ranh),
                        bottomSheet.getWidth(), 250, 250, 250));
            }

            loadingIndicator.smoothToHide();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpLayoutQHCT(QHChiTiet QHCT) {
        tvQHChiTiet.setVisibility(GONE);
        tvQHChiTiet1.setVisibility(GONE);
        tvTenDuAn.setText(QHCT.getTenDuAn());
        tvQuanQHCT.setText("Quận Thủ Đức");

        if (thuaDatInfo.getMaPhuongXa().equals("26824"))
            tvPhuongQHCT.setText("Phường Bình Thọ");
        else if (thuaDatInfo.getMaPhuongXa().equals("26815"))
            tvPhuongQHCT.setText("Phường Linh Chiểu");
        else tvPhuongQHCT.setText("Phường Linh Tây");

        tvDienTichQHCT.setText("Khoảng " + QHCT.getDienTichRanh());
        tvTrangThai.setText(QHCT.getTrangThaiQuyHoach());
        tvCoQuanPheDuyet.setText(QHCT.getCoQuanPheDuyet());
        tvSoQuyetDinh.setText(QHCT.getSoQuyetDinh());
    }

    private void parseGeoThuaDat(JSONObject jsonObj) {
        try {
            JSONArray featureList = new JSONArray();
            JSONObject result = new JSONObject();
            JSONObject point = new JSONObject(); // geometry feature
            point.put("type", "Polygon");
            point.put("coordinates", LocationHelper.parseCoordinates(jsonObj.getString("Ranh")));

            JSONObject properties = new JSONObject(); // properties feature
            properties.put("MaThuaDat", jsonObj.getString("MaThuaDat"));
            JSONObject feature = new JSONObject();
            feature.put("type", "Feature");
            feature.put("id", jsonObj.getString("MaThuaDat"));
            feature.put("geometry", point);
            feature.put("properties", properties);
            featureList.put(feature);
            result.put("type", "FeatureCollection");
            result.put("features", featureList);

            ArrayList<LatLng> coordinates = new ArrayList<>();
            JSONArray RanhArray = new JSONArray(jsonObj.getString("Ranh"));
            RanhArray = RanhArray.getJSONArray(0).getJSONArray(0);

            for (int j = 0; j < RanhArray.length(); j++) { // get coordinates of thua dat polygon
                LatLng latlng = new LatLng((Double)RanhArray.getJSONArray(j).get(1),
                        (Double)(RanhArray.getJSONArray(j).get(0)));
               coordinates.add(latlng);
            }

            thuadatSource = new GeoJsonSource("thuadat-src", result.toString());
            mMap.addSource(thuadatSource);
            thuadat = new FillLayer("thuadat-layer", "thuadat-src");
            thuadat.setProperties(
                    PropertyFactory.fillColor(Color.rgb(100, 149, 237))
            );
            thuaDatStroke = new LineLayer("thuadat-stroke", "thuadat-src");
            thuaDatStroke.setProperties(
                    PropertyFactory.lineColor(Color.rgb(0, 0, 255)),
                    PropertyFactory.lineWidth(2f)
            );
            mMap.addLayer(thuadat);
            mMap.addLayer(thuaDatStroke); // Draw stroke around polygon
            coordinatesThuaDat = LocationHelper.parseCoordinates(jsonObj.getString("Ranh")).getJSONArray(0);

            if (DeviceSizeHelper.getRotation(getApplicationContext()) == 0) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                        250, 250, 250, bottomSheet.getHeight()));
            } else if (DeviceSizeHelper.getRotation(getApplicationContext()) == 1) {
               mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LocationHelper.getCenterBounds(coordinatesThuaDat),
                        bottomSheet.getWidth(), 250, 250, 250));
            }
            loadingIndicator.smoothToHide();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getJsonQuyHoachSDD(String URL) {
        StringRequest str = new StringRequest(Request.Method.GET,
                URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null && !response.equals("[]")) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);

                        if (jsonArray.length() > 0) {
                            try {
                                if (mMap != null && thuadat != null && thuaDatStroke != null) {
                                    if (mMap.getLayer(thuadat.getId()) != null) {
                                        mMap.removeLayer(thuadat);
                                    }

                                    if (mMap.getLayer(thuaDatStroke.getId()) != null) {
                                        mMap.removeLayer(thuaDatStroke);
                                    }
                                    thuadat = null;
                                    thuaDatStroke = null;
                                }
                            } catch (CannotAddLayerException e) {
                                e.printStackTrace();
                            }

                            try {
                                if (mMap != null && thuadatSource != null && mMap.getSource(thuadatSource.getId()) != null) { // Remove polygon thua dat before add polygons sdd dat
                                    mMap.removeSource(thuadatSource);
                                    thuadatSource = null;
                                }
                            } catch (CannotAddSourceException e) {
                                e.printStackTrace();
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                    String json = "[" + jsonArray.getJSONObject(i).toString() + "]";
                                    List<QHInfo> QHList = Arrays.asList(gson.fromJson(json, QHInfo[].class));
                                    QHListAdapter.add(QHList.get(0));
                                }
                                QHListAdapter.notifyDataSetChanged();

                                isBtnXemQHClicked = true;
                                markers = new HashMap<>();
                                markersOrder = new HashMap<>();
                                markersArray = new ArrayList<>();
                                sddLayerList = new ArrayList<>();
                                sddStrokeList = new ArrayList<>();
                                sddSourceList = new ArrayList<>();
                                jsonSdd = jsonArray;
                                parseGeoSDDThuaDat(jsonArray);
                            }
                        } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    DialogHelper.showErrorDialog(MainActivity.this, "Không tìm thấy thông tin quy hoạch",
                            "Không tìm thấy thông tin quy hoạch. Hãy kiểm tra lại thông tin");
                    isBtnXemQHClicked = false;
                    loadingIndicator.smoothToHide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingIndicator.smoothToHide();
                isBtnXemQHClicked = false;
                Toast.makeText(getApplicationContext(), "Không có kết nối đến server. Hãy thử lại!", Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue().add(str);
    }

    private void parseGeoSDDThuaDat(JSONArray jsonArray)
    {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject result = new JSONObject();
                JSONObject jsObj = jsonArray.getJSONObject(i);
                JSONArray featureList = new JSONArray();
                JSONObject point = new JSONObject(); // geometry feature
                point.put("type", "Polygon");

                // Check for multipolygon
                String ranh = jsObj.getString("Ranh");
                ranh = ranh.substring(1, ranh.length() - 1);

                if (ranh.contains("]]],[[[")) {
                    String [] coordRanh = ranh.split("\\]\\]\\],\\[\\[\\[");
                    point.put("coordinates", new JSONArray(coordRanh[0] + "]]]")); // take first element to draw marker

                    int n = coordRanh.length - 1;
                    String[] temp = new String[n];
                    System.arraycopy(coordRanh, 1, temp, 0, n);

                    if (temp.length > 1) { // modified other elements

                        for (int j = 0; j < temp.length - 1; j++) {
                            temp[j] = "[[[" + temp[j] + "]]]";
                        }
                        temp[temp.length - 1] = "[[[" + temp[temp.length - 1];
                    }
                    else {
                        temp[0] = "[[[" + temp[0];
                    }
                    DrawMultiPolygon(temp, i + 1, jsObj.getString("RGBColor")); // now draw these elements
                }
                else {
                    point.put("coordinates", LocationHelper.parseCoordinates(jsObj.getString("Ranh")));
                }

                JSONObject properties = new JSONObject(); // properties feature
                properties.put("SoThuTu", i + 1);
                properties.put("DienTich", jsObj.getString("DienTich"));
                properties.put("PhanTramDT", jsObj.getString("PhanTramDT"));
                properties.put("ChucNangSDD", jsObj.getString("ChucNangSDD"));
                properties.put("RGBColor", "rgba(" + jsObj.getString("RGBColor") + ",0.7)");
                properties.put("MatDo", jsObj.getString("MatDo"));
                properties.put("TangCao", jsObj.getString("TangCao"));
                properties.put("HeSoDD", jsObj.getString("HeSoSDD"));

                JSONObject feature = new JSONObject();
                feature.put("type", "Feature");
                feature.put("geometry", point);
                feature.put("properties", properties);
                featureList.put(feature);
                result.put("type", "FeatureCollection");
                result.put("features", featureList);

                String RGBColor = jsObj.getString("RGBColor");
                String [] colors = RGBColor.split(",");
                Source sddSource = new GeoJsonSource("sdd" + String.valueOf(i + 1) + "-src", result.toString());
                mMap.addSource(sddSource);
                sddSourceList.add(sddSource);

                LineLayer sddStroke = new LineLayer("sdd" + String.valueOf(i + 1) + "stroke", "sdd" +
                String.valueOf(i + 1) + "-src"); // Draw stroke around polygon
                sddStroke.setProperties(
                        PropertyFactory.lineWidth(2f),
                        PropertyFactory.lineColor(Color.BLACK)
                );

                FillLayer sdd = new FillLayer("sdd" + String.valueOf(i + 1) + "layer", "sdd" + String.valueOf(i + 1) + "-src");
                sdd.setProperties(
                        PropertyFactory.fillColor(Color.rgb(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]), Integer.valueOf(colors[2]))),
                        PropertyFactory.fillOpacity(0.8f)
                ); // Draw sdd polygon
                sddLayerList.add(sdd);
                sddStrokeList.add(sddStroke);
                mMap.addLayer(sdd);
                mMap.addLayer(sddStroke);

                JSONArray coordinates = LocationHelper.parseCoordinates(jsObj.getString("Ranh")).getJSONArray(0);
                String json = "[" + jsonArray.getJSONObject(i).toString() + "]";
                List<QHInfo> QHList = Arrays.asList(gson.fromJson(json, QHInfo[].class)); // Get sdd dat info
                QHInfo qhInfo = QHList.get(0);

                // Custom markers with numbers
                View marker = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
                TextView edtNumber = (TextView) marker.findViewById(R.id.edtNumber);

                if (i >= 10) { // Move number in marker to left when that number > 9
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) edtNumber.getLayoutParams();
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(lp.leftMargin - 6, lp.topMargin, 0, 0);
                    edtNumber.setLayoutParams(params);
                }
                edtNumber.setText(String.valueOf(i + 1));

                // Add marker to sdd dat polygon
                IconFactory iconFactory = IconFactory.getInstance(MainActivity.this);
                Icon icon = iconFactory.fromBitmap(BitmapHelper.createDrawableFromView(this, marker));

                Marker mMarker = mMap.addMarker(new MarkerViewOptions()
                        .icon(icon)
                        .position(LocationHelper.getMarkerPosition(coordinates)));
                markersArray.add(mMarker); // Save marker to remove when click btnTimMoi
                markers.put(mMarker.getId(), qhInfo);
                markersOrder.put(mMarker.getId(), i + 1);
            }
            loadingIndicator.smoothToHide();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void DrawMultiPolygon(String []temp, int number, String color) {
        try {
            for (int i = 0; i < temp.length; i++) {
                JSONObject result = new JSONObject();
                JSONArray featureList = new JSONArray();
                JSONObject point = new JSONObject(); // geometry feature
                point.put("type", "Polygon");
                point.put("coordinates", new JSONArray(temp[i]));

                JSONObject properties = new JSONObject(); // properties feature
                properties.put("SoThuTu", i + 1);

                JSONObject feature = new JSONObject();
                feature.put("type", "Feature");
                feature.put("geometry", point);
                feature.put("properties", properties);
                featureList.put(feature);
                result.put("type", "FeatureCollection");
                result.put("features", featureList);

                String[] colors = color.split(",");
                Source sddSource = new GeoJsonSource("sdd" + String.valueOf(number) + "." +
                        String.valueOf(i + 1) + "-src", result.toString());
                mMap.addSource(sddSource);
                sddSourceList.add(sddSource);

                LineLayer sddStroke = new LineLayer("sdd" + String.valueOf(number) + "." +
                        String.valueOf(i + 1) + "stroke", "sdd" + String.valueOf(number) + "." +
                        String.valueOf(i + 1) + "-src");      // Draw stroke around polygon
                sddStroke.setProperties(
                        PropertyFactory.lineWidth(2f),
                        PropertyFactory.lineColor(Color.BLACK)
                );

                FillLayer sdd = new FillLayer("sdd" + String.valueOf(number) + "." +
                        String.valueOf(i + 1) + "layer", "sdd" + String.valueOf(number) + "." +
                        String.valueOf(i + 1) + "-src");
                sdd.setProperties(
                        PropertyFactory.fillColor(Color.rgb(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]), Integer.valueOf(colors[2]))),
                        PropertyFactory.fillOpacity(0.8f)
                ); // Draw sdd polygon
                sddLayerList.add(sdd);
                sddStrokeList.add(sddStroke);
                mMap.addLayer(sdd);
                mMap.addLayer(sddStroke);
            }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    private void setUpLayoutXemThongTinThua(ThuaDatInfo thuaDat) {
        tvTP.setText("TP. Hồ Chí Minh");

        if (thuaDat.getMaQuanHuyen().equals("762"))
            tvQuan.setText("Quận Thủ Đức");

        if (thuaDat.getMaPhuongXa().equals("26824"))
            tvPhuong.setText("Phường Bình Thọ");
        else if (thuaDat.getMaPhuongXa().equals("26815"))
            tvPhuong.setText("Phường Linh Chiểu");
        else tvPhuong.setText("Phường Linh Tây");

        tvSoThua.setText(thuaDat.getSoThua());
        tvSoTo.setText(thuaDat.getSoTo());
        tvDienTich.setText("Khoảng " + thuaDat.getDienTich());
        tvQHPhanKhu.setText(thuaDat.getTenDoAn());

        tvQHChiTiet.setVisibility(VISIBLE);
        tvQHChiTiet1.setVisibility(VISIBLE);
        if (thuaDat.getTenDuAn() == null) {
            tvQHChiTiet.setTextColor(Color.BLACK);
            tvQHChiTiet.setText("Không có quy hoạch chi tiết");
        }
        else {
            tvQHChiTiet.setTextColor(Color.BLUE);
            tvQHChiTiet.setText(thuaDat.getTenDuAn());
            tvQHChiTiet.setOnClickListener(this);
        }
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
           // if (!locationServices.areLocationPermissionsGranted()) {
            if (!PermissionsManager.areLocationPermissionsGranted(MainActivity.this)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) ) {
                    showLocationRequestDialog();

                } else if (locationPermissionStatus.getBoolean("locationPermission", false)) {
                    showGPSRequestDialog();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
                }

                SharedPreferences.Editor editor = locationPermissionStatus.edit();
                editor.putBoolean("locationPermission", true);
                editor.commit();
            } else {
                if (!LocationHelper.isLocationServiceEnabled(getApplicationContext())) {
                    showGPSRequestDialog();
                }
                else enableLocation(true);
            }
        } else {
            if (!LocationHelper.isLocationServiceEnabled(getApplicationContext())) {
                showGPSRequestDialog();
            }

            if (isOff) {
                enableLocation(false);
            } else  {
                enableLocation(true);
                isOff = true;
            }
        }
    }

    private void showLocationRequestDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Thông báo");
        builder.setMessage("Ứng dụng cần quyền truy cập vị trí.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showGPSRequestDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Thông báo");
        builder.setMessage("Ứng dụng cần bật chức năng GPS của thiết bị.");
        builder.setPositiveButton("Bật GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, REQUEST_PERMISSION_SETTING);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showStorageRequestDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Cần quyền truy cập bộ nhớ");
        builder.setMessage("Ứng dụng cần quyền truy cập bộ nhớ thiết bị của bạn.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static void showStorageRequestDialog1(final Activity activity, final String[] permissionsRequired,
                                                 final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Cần quyền truy cập bộ nhớ");
        builder.setMessage("Ứng dụng cần quyền truy cập bộ nhớ thiết bị của bạn.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ActivityCompat.requestPermissions(activity, permissionsRequired,
                        requestCode);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            Location lastLocation = locationEngine.getLastLocation();
           // Location lastLocation = locationServices.getLastLocation();

            if (lastLocation != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
            }

            locationEngine.addLocationEngineListener(new LocationEngineListener() {
                @Override
                public void onConnected() {
                    locationEngine.requestLocationUpdates();
                    Toast.makeText(getApplicationContext(), "Đang lấy vị trí hiện tại...", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationEngine.removeLocationEngineListener(this);
                    }
                }
            });
            locationEngine.addLocationEngineListener(locationEngineListener);

           /* locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                    locationServices.removeLocationListener(this);
                }
            }); */
            btnLocation.setImageResource(R.drawable.ic_location_disabled);
        } else {
            btnLocation.setImageResource(R.drawable.ic_location);
        }

        mMap.setMyLocationEnabled(enabled);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (!LocationHelper.isLocationServiceEnabled(getApplicationContext())) {
                    showGPSRequestDialog();
                }
                enableLocation(true);
                isOff = true;
            }
            else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showLocationRequestDialog();
                }
            }
        }

        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allgranted = false;

            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                FileHelper.saveFile(this, maThuaDat, thuaDatInfo);
            }
            else if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[1])) {
                showStorageRequestDialog1(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }
            else {
                Toast.makeText(getBaseContext(),"Không thể lấy quyền truy cập bộ nhớ!",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PERMISSION_SETTING) {
           // if (!locationServices.areLocationPermissionsGranted()) {
            if (!PermissionsManager.areLocationPermissionsGranted(MainActivity.this)) {
                enableLocation(true);
                isOff = true;
            } else {
                showLocationRequestDialog();
            }

            if (permissionsRequired != null) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                    FileHelper.saveFile(this, maThuaDat, thuaDatInfo);
                }
                else {
                    showStorageRequestDialog1(MainActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);

        if (diag.isShowing()) {
            outState.putBoolean("isDiagHelpShowing", true);
        }

        if (!isHelpFinished) {
            outState.putBoolean("isHelpFinished", false);
            outState.putInt("dialogNumber", dialogNumber);
        }

        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            outState.putBoolean("isBottomSheetExpanding", true);
        }

        if (jsonThuaDat != null) {
            outState.putBoolean("isBtnSearchClicked", isBtnSearchClicked);
            outState.putString("jsonThuaDat", jsonThuaDat.toString());
        }

        if (jsonSdd != null) {
            outState.putBoolean("isBtnXemQHClicked", isBtnXemQHClicked);
            outState.putString("jsonSdd", jsonSdd.toString());
        }

        if (jsonQHCT != null) {
            outState.putBoolean("isXemQHCT", isXemQHCT);
            outState.putString("jsonQHCT", jsonQHCT.toString());
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

        if (locationEngineListener != null) {
            locationEngine.removeLocationEngineListener(locationEngineListener);
        }
    }

   @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

  /*  @Override
    public void onBackPressed() {
        if (backPressedTime + period > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Nhấn Back lần nữa để thoát ứng dụng", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }*/
}
