package vlab.thongtinquyhoachthuduc.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import vlab.thongtinquyhoachthuduc.R;
import vlab.thongtinquyhoachthuduc.Utils.LocationHelper;

public class SplashScreen extends AppCompatActivity {

    private Timer timer;
    private ProgressBar progressBar;
    private int i = 0;
    private boolean checkCon = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (!LocationHelper.isNetworkConnected(this)) {
            Toast.makeText(getApplicationContext(), "Hãy kết nối internet để sử dụng ứng dụng",
                    Toast.LENGTH_SHORT).show();
        }

        final Thread checkConnection = new Thread(new Runnable() {
            @Override
            public void run() {

                while (checkCon == false) {
                    if (!LocationHelper.isNetworkConnected(SplashScreen.this)) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkCon = false;
                                Toast.makeText(SplashScreen.this, "Hãy kết nối Internet để sử dụng ứng dụng", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        checkCon = true;
                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        final long period = 100;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (i < 100) {
                    progressBar.setProgress(i);
                    i += 10;
                } else {
                    timer.cancel();

                    if (!LocationHelper.isNetworkConnected(SplashScreen.this)) {
                        checkConnection.start();
                    } else {
                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, 0, period);
    }

}
