package vlab.thongtinquyhoachthuduc.Model;

public class QHChiTiet {
    String TenDuAn;
    String MaQHCTThuaDat;
    String MaQHCTRanh;
    String DienTichRanh;
    String TrangThaiQuyHoach;
    String CoQuanPheDuyet;
    String SoQuyetDinh;
    String NgayDuyet;
    String RanhQHCT;
    String RanhQHCTThuaDat;

    public String getTenDuAn() {
        return TenDuAn;
    }

    public void setTenDuAn(String tenDuAn) {
        TenDuAn = tenDuAn;
    }

    public String getMaQHCTThuaDat() {
        return MaQHCTThuaDat;
    }

    public void setMaQHCTThuaDat(String maQHCTThuaDat) {
        MaQHCTThuaDat = maQHCTThuaDat;
    }

    public String getMaQHCTRanh() {
        return MaQHCTRanh;
    }

    public void setMaQHCTRanh(String maQHCTRanh) {
        MaQHCTRanh = maQHCTRanh;
    }

    public String getDienTichRanh() {
        return DienTichRanh;
    }

    public void setDienTichRanh(String dienTichRanh) {
        DienTichRanh = dienTichRanh;
    }

    public String getTrangThaiQuyHoach() {
        return TrangThaiQuyHoach;
    }

    public void setTrangThaiQuyHoach(String trangThaiQuyHoach) {
        TrangThaiQuyHoach = trangThaiQuyHoach;
    }

    public String getCoQuanPheDuyet() {
        return CoQuanPheDuyet;
    }

    public void setCoQuanPheDuyet(String coQuanPheDuyet) {
        CoQuanPheDuyet = coQuanPheDuyet;
    }

    public String getSoQuyetDinh() {
        return SoQuyetDinh;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        SoQuyetDinh = soQuyetDinh;
    }

    public String getNgayDuyet() {
        return NgayDuyet;
    }

    public void setNgayDuyet(String ngayDuyet) {
        NgayDuyet = ngayDuyet;
    }

    public String getRanhQHCT() {
        return RanhQHCT;
    }

    public void setRanhQHCT(String ranhQHCT) {
        RanhQHCT = ranhQHCT;
    }

    public String getRanhQHCTThuaDat() {
        return RanhQHCTThuaDat;
    }

    public void setRanhQHCTThuaDat(String ranhQHCTThuaDat) {
        RanhQHCTThuaDat = ranhQHCTThuaDat;
    }
}
