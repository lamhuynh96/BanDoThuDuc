package vlab.thongtinquyhoachthuduc.Model;

public class QHPhanKhuInfo {
    String MaQHPKSDD;
    String MaSo;
    String RGBColor;
    String Ranh;

    public String getMaQHPKSDD() {
        return MaQHPKSDD;
    }

    public void setMaQHPKSDD(String maQHPKSDD) {
        MaQHPKSDD = maQHPKSDD;
    }

    public String getMaSo() {
        return MaSo;
    }

    public void setMaSo(String maSo) {
        MaSo = maSo;
    }

    public String getRGBColor() {
        return RGBColor;
    }

    public void setRGBColor(String RGBColor) {
        this.RGBColor = RGBColor;
    }

    public String getRanh() {
        return Ranh;
    }

    public void setRanh(String ranh) {
        Ranh = ranh;
    }
}
