package vlab.thongtinquyhoachthuduc.Model;

public class ThuaDatInfo {
    String MaThuaDat;
    String SoTo;
    String SoThua;
    String DienTich;
    String SoNha;
    String TenDuong;
    String TenPhuongXa;
    String Ranh;
    String MaPhuongXa;
    String MaQuanHuyen;
    String TenQuanHuyen;
    String TenDoAn;
    String SoQuyetDinh;
    String CoQuanPheDuyet;
    String NgayDuyet;
    String TenDuAn;

    public String getMaThuaDat() {
        return MaThuaDat;
    }

    public void setMaThuaDat(String maThuaDat) {
        MaThuaDat = maThuaDat;
    }

    public String getSoTo() {
        return SoTo;
    }

    public void setSoTo(String soTo) {
        SoTo = soTo;
    }

    public String getSoThua() {
        return SoThua;
    }

    public void setSoThua(String soThua) {
        SoThua = soThua;
    }

    public String getDienTich() {
        return DienTich;
    }

    public void setDienTich(String dienTich) {
        DienTich = dienTich;
    }

    public String getSoNha() {
        return SoNha;
    }

    public void setSoNha(String soNha) {
        SoNha = soNha;
    }

    public String getTenDuong() {
        return TenDuong;
    }

    public void setTenDuong(String tenDuong) {
        TenDuong = tenDuong;
    }

    public String getTenPhuongXa() {
        return TenPhuongXa;
    }

    public void setTenPhuongXa(String tenPhuongXa) {
        TenPhuongXa = tenPhuongXa;
    }

    public String getRanh() {
        return Ranh;
    }

    public void setRanh(String ranh) {
        Ranh = ranh;
    }

    public String getMaPhuongXa() {
        return MaPhuongXa;
    }

    public void setMaPhuongXa(String maPhuongXa) {
        MaPhuongXa = maPhuongXa;
    }

    public String getMaQuanHuyen() {
        return MaQuanHuyen;
    }

    public void setMaQuanHuyen(String maQuanHuyen) {
        MaQuanHuyen = maQuanHuyen;
    }

    public String getTenQuanHuyen() {
        return TenQuanHuyen;
    }

    public void setTenQuanHuyen(String tenQuanHuyen) {
        TenQuanHuyen = tenQuanHuyen;
    }

    public String getTenDoAn() {
        return TenDoAn;
    }

    public void setTenDoAn(String tenDoAn) {
        TenDoAn = tenDoAn;
    }

    public String getSoQuyetDinh() {
        return SoQuyetDinh;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        SoQuyetDinh = soQuyetDinh;
    }

    public String getCoQuanPheDuyet() {
        return CoQuanPheDuyet;
    }

    public void setCoQuanPheDuyet(String coQuanPheDuyet) {
        CoQuanPheDuyet = coQuanPheDuyet;
    }

    public String getNgayDuyet() {
        return NgayDuyet;
    }

    public void setNgayDuyet(String ngayDuyet) {
        NgayDuyet = ngayDuyet;
    }

    public String getTenDuAn() {
        return TenDuAn;
    }

    public void setTenDuAn(String tenDuAn) {
        TenDuAn = tenDuAn;
    }
}

