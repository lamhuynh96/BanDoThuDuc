package vlab.thongtinquyhoachthuduc.Model;

public class QHInfo {
    String MaQHPKSDDThuaDat;
    String MaQHPKSDD;
    String MaQHPKRanh;
    String MaOPho;
    String DanSo;
    String HeSoSDD;
    String MatDo;
    String TangCao;
    String ChucNangSDD;
    String RGBColor;
    String IndexColor;
    String DienTich;
    String PhanTramDT;
    String Ranh;

    public String getMaQHPKSDDThuaDat() {
        return MaQHPKSDDThuaDat;
    }

    public void setMaQHPKSDDThuaDat(String maQHPKSDDThuaDat) {
        MaQHPKSDDThuaDat = maQHPKSDDThuaDat;
    }

    public String getMaQHPKSDD() {
        return MaQHPKSDD;
    }

    public void setMaQHPKSDD(String maQHPKSDD) {
        MaQHPKSDD = maQHPKSDD;
    }

    public String getMaQHPKRanh() {
        return MaQHPKRanh;
    }

    public void setMaQHPKRanh(String maQHPKRanh) {
        MaQHPKRanh = maQHPKRanh;
    }

    public String getMaOPho() {
        return MaOPho;
    }

    public void setMaOPho(String maOPho) {
        MaOPho = maOPho;
    }

    public String getDanSo() {
        return DanSo;
    }

    public void setDanSo(String danSo) {
        DanSo = danSo;
    }

    public String getHeSoSDD() {
        return HeSoSDD;
    }

    public void setHeSoSDD(String heSoSDD) {
        HeSoSDD = heSoSDD;
    }

    public String getMatDo() {
        return MatDo;
    }

    public void setMatDo(String matDo) {
        MatDo = matDo;
    }

    public String getTangCao() {
        return TangCao;
    }

    public void setTangCao(String tangCao) {
        TangCao = tangCao;
    }

    public String getChucNangSDD() {
        return ChucNangSDD;
    }

    public void setChucNangSDD(String chucNangSDD) {
        ChucNangSDD = chucNangSDD;
    }

    public String getIndexColor() {
        return IndexColor;
    }

    public void setIndexColor(String indexColor) {
        IndexColor = indexColor;
    }

    public String getDienTich() {
        return DienTich;
    }

    public void setDienTich(String dienTich) {
        DienTich = dienTich;
    }

    public String getPhanTramDT() {
        return PhanTramDT;
    }

    public void setPhanTramDT(String phanTramDT) {
        PhanTramDT = phanTramDT;
    }

    public String getRanh() {
        return Ranh;
    }

    public void setRanh(String ranh) {
        Ranh = ranh;
    }


    public String getRGBColor() {
        return RGBColor;
    }

    public void setRGBColor(String RGBColor) {
        this.RGBColor = RGBColor;
    }

}
