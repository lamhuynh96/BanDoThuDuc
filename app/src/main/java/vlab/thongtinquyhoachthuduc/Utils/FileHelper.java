package vlab.thongtinquyhoachthuduc.Utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import vlab.thongtinquyhoachthuduc.Model.ThuaDatInfo;
import vlab.thongtinquyhoachthuduc.R;

import static android.content.Context.DOWNLOAD_SERVICE;

public class FileHelper {
    public static void saveFile(Context context, String maThuaDat, ThuaDatInfo thuaDatInfo) {
        String url = context.getString(R.string.download_thuadat_info) + maThuaDat + "/thong-tin-quy-hoach";
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        // File name like: 23022017_2_3_BinhTho.pdf
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        String fileName = "";
        fileName += df.format(c.getTime());

        if (thuaDatInfo != null) {
            fileName += "_" + thuaDatInfo.getSoThua() + "_" + thuaDatInfo.getSoTo();
            String maPhuong = thuaDatInfo.getMaPhuongXa();

            if (maPhuong.equals("26824")) {
                fileName += "_BinhTho";
            } else if (maPhuong.equals("26815")) {
                fileName += "_LinhChieu";
            } else {
                fileName += "_LinhTay";
            }
        }

        request.setDescription(fileName + ".pdf");
        request.setTitle(fileName + ".pdf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName + ".pdf");
        DownloadManager manager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
}
