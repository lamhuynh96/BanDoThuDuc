package vlab.thongtinquyhoachthuduc.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.LinearLayout;

import vlab.thongtinquyhoachthuduc.R;

public class BaseCustomDialog extends Dialog {
    public BaseCustomDialog(Context context){
        super(context);
        getWindow().setBackgroundDrawableResource(R.drawable.button_normal);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }
}
