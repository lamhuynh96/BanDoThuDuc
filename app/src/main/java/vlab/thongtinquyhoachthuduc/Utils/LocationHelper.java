package vlab.thongtinquyhoachthuduc.Utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

public class LocationHelper {
    public static boolean isLocationServiceEnabled(Context context){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if (locationManager == null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ex){}

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        catch(Exception ex){}
        return gps_enabled || network_enabled;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    public static LatLngBounds getCenterBounds(JSONArray coordinates) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLngBounds bounds;

        for (int i = 0; i < coordinates.length() - 1; i++) {
            try {
                builder.include(new LatLng((Double)coordinates.getJSONArray(i).get(1),
                        (Double)(coordinates.getJSONArray(i).get(0))));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        bounds = builder.build();
        return bounds;
    }

    public static LatLng getMarkerPosition(JSONArray coordinates) {
        LatLng position = getCenterBounds(coordinates).getCenter();
        Double x = position.getLongitude();
        Double y = position.getLatitude();
        boolean inside = false;

        for (int i = 0, j = coordinates.length() - 1; i < coordinates.length(); j = i++) {
            try {
                Double xi = (Double) coordinates.getJSONArray(i).get(0);
                Double yi = (Double) coordinates.getJSONArray(i).get(1);
                Double xj = (Double) coordinates.getJSONArray(j).get(0);
                Double yj = (Double) coordinates.getJSONArray(j).get(1);

                boolean intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) {
                    inside = !inside;
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!inside) {
            int pos = (int) Math.floor(Math.random() * coordinates.length());
            try {
                position = new LatLng((Double) coordinates.getJSONArray(pos).get(1),
                        (Double) coordinates.getJSONArray(pos).get(0));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return position;
    }

    public static JSONArray parseCoordinates(String text) {
        JSONArray res = new JSONArray();
        try {
            res = new JSONArray(text).getJSONArray(0);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }
}
