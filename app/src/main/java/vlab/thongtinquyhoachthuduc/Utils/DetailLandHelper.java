package vlab.thongtinquyhoachthuduc.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import vlab.thongtinquyhoachthuduc.Activity.MainActivity;
import vlab.thongtinquyhoachthuduc.Adapter.DetailListAdapter;
import vlab.thongtinquyhoachthuduc.R;

public class DetailLandHelper {
    public static final String[] typeLand = {
            "Đất nhóm nhà ở",
            "Đất ở liền kề",
            "Đất ở chung cư",
            "Đất ở hỗn hợp",
            "Đất ở biệt thự",
            "Đất ở làng xóm",
            "Đất công cộng đô thị",
            "Đất công cộng đơn vị ở",
            "Đất thương mại - dịch vụ",
            "Đất hỗn hợp",
            "Đất văn hóa",
            "Đất trường THPT",
            "Đất trường THCS, tiểu học, mầm non",
            "Đất cây xanh đô thị",
            "Đất cây xanh đơn vị ở",
            "Đất cây xanh chuyên đề",
            "Đất trung tâm TDTT",
            "Đất cây xanh cách ly",
            "Đất công nghiệp",
            "Đất kho tàng, bến bãi",
            "Đất trung tâm nghiên cứu đào tạo",
            "Đất cơ quan, công ty",
            "Đất trung tâm y tế",
            "Đất du lịch",
            "Đất tôn giáo, di tích",
            "Đất công trình đầu mối hạ tầng kỹ thuật",
            "Đất an ninh quốc phòng",
            "Đất nghĩa trang",
            "Đất nông nghiệp",
            "Đất lâm nghiệp",
            "Mặt nước",
            "Đất giao thông"
    } ;
    public static final String[] colorInHex = {
            "#a57c00",
            "#a55200",
            "#ffbf00",
            "#7F5F00",
            "#4C4C00",
            "#4C4C26",
            "#ff0000",
            "#ff0000",
            "#ff7f7f",
            "#FF9F7F",
            "#ff00ff",
            "#7f1f00",
            "#7f3f3f",
            "#52A500",
            "#264C00",
            "#007F00",
            "#007F00",
            "#004C39",
            "#5200a5",
            "#26267e",
            "#005F7F",
            "#7F3F00",
            "#ff00bf",
            "#ff00ff",
            "#4C0000",
            "#264C00",
            "#232613",
            "#656565",
            "#00ff00",
            "#002626",
            "#0052a5",
            "#828282"
    };

    public static AlertDialog addGlossary(Activity activity) {
        final LayoutInflater factory = activity.getLayoutInflater();
        final View textEntryView = factory.inflate(R.layout.list_details, null);
        ListView list = (ListView)textEntryView.findViewById(R.id.lv);
        DetailListAdapter adapter = new DetailListAdapter(list.getContext(),
                DetailLandHelper.typeLand,
                DetailLandHelper.colorInHex);
        list.setAdapter(adapter);

        ((ViewGroup)list.getParent()).removeView(list);

        TextView title = new TextView(activity);
        title.setText("Phân loại chức năng sử dụng đất");
        title.setBackgroundColor(Color.parseColor("#263238"));
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(15);

        AlertDialog diag = new AlertDialog.Builder(activity)
                .setCustomTitle(title)
                .setView(list)
                .create();
        diag.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        return diag;
    }

}
